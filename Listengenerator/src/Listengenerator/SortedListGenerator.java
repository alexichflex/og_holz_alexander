package Listengenerator;

import java.util.Random;

public class SortedListGenerator {

	public static long[] getSortedList(int laenge) {
		Random rand = new Random(111111);
		long[] zahlenliste = new long[laenge];
		long naechsteZahl = 0;

		for (int i = 0; i < laenge; i++) {
			naechsteZahl += rand.nextInt(3) + 1;
			zahlenliste[i] = naechsteZahl;
		}

		return zahlenliste;
	}

	static void binaerSuche(long[] feld, int links, int rechts, long kandidat) {
		int mitte;
		do {

			mitte = (rechts + links) / 2;
			if (feld[mitte] < kandidat) {
				links = mitte + 1;
			} else {
				rechts = mitte - 1;
			}
		} while (feld[mitte] != kandidat && links <= rechts);
		if (feld[mitte] == kandidat) {
			System.out.println("Position: " + mitte);
		} else {
			System.out.println("Wert nicht vorhanden!");
		}
	}
	
	static void linearSuche(long[] feld, long kandidat) {
		for (int i = 0; i < feld.length; i++)
			if (feld[i] == kandidat) return;
	}

	public static void main(String[] args) {
		final int ANZAHL = 20000000; // 20.000.000

		long[] a = getSortedList(ANZAHL);
		long gesuchteZahl = a[ANZAHL -9000000];
		System.out.println(gesuchteZahl);
		
		long time = System.currentTimeMillis();
		linearSuche(a, gesuchteZahl);
		System.out.println(System.currentTimeMillis() - time + "ms");

		// for(long x: a)
		// System.out.println(x);

		long gesZahl = a[ANZAHL-1];
		
		long time1 = System.currentTimeMillis();
		binaerSuche(a, 0, ANZAHL, gesZahl);
		System.out.println(System.currentTimeMillis() - time1 + "ms");
	}
}