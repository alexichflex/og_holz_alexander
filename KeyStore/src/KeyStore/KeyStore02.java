package KeyStore;

import java.util.ArrayList;

public class KeyStore02 {

	ArrayList<String> liste = new ArrayList<String>();

	public boolean add(String eintrag) {
		return liste.add(eintrag);
	}

	public int indexOf(String eingabe) {
		return liste.indexOf(eingabe);
	}

	public boolean remove(int eingabe) {
		return liste.remove(eingabe) != null;
	}

	@Override
	public String toString() {
		return "KeyStore02 [liste=" + liste + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

}