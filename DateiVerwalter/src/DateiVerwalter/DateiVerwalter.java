package DateiVerwalter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class DateiVerwalter {

	private File file;

	public DateiVerwalter(File file) {
		this.file = file;
	}

	public void schreiben(String s) {
		try {
			FileWriter fw = new FileWriter(this.file, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.newLine();
			bw.write(s);
			bw.flush();
			bw.close();
		} catch (IOException e) {
			System.out.println("File wahrscheinlich nicht vorhanden");
			e.printStackTrace();
		}
	}

	public void lesen() {
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader br = new BufferedReader(fr);
			String s;
			while ((s = br.readLine()) != null) {
				System.out.println(s);
			}
			br.close();
		} catch (Exception e) {
			System.out.println("File wahrscheinlich nicht vorhanden");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File file = new File("OG 8.txt");
		DateiVerwalter dv = new DateiVerwalter(file);
		dv.schreiben("Fabien");
	}
}