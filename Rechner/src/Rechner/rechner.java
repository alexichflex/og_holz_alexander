package Rechner;

import java.io.IOException;
import java.util.Scanner;

public class rechner {
    public static void main(String[] args) throws IOException{
        while (true)
        {
            String eingabe1;
            int zahl1, zahl2;

            System.out.println("Geben Sie eine ganze Zahl ein (x zum beenden): ");
            eingabe1 = new Scanner(System.in).nextLine();

            if (eingabe1.equalsIgnoreCase("x"))
                break;
            try {
            	zahl1 = Integer.parseInt(eingabe1);

            	System.out.println("Geben Sie eine ganze Zahl ein: ");
            	zahl2 = Integer.parseInt(new Scanner(System.in).nextLine());
			
            	int zahl3 = zahl1+zahl2;
            	System.out.printf("%d + %d = %d%n", zahl1, zahl2, zahl1 + zahl2);
            	System.out.printf("%d - %d = %d%n", zahl1, zahl2, zahl1 - zahl2);
            	System.out.printf("%d * %d = %d%n", zahl1, zahl2, zahl1 * zahl2);
            	System.out.printf("%d / %d = %d%n", zahl1, zahl2, zahl1 / zahl2);
            }
            catch (NumberFormatException e) {
				System.out.println("Eingabe ist keine Zahl!");
				e.printStackTrace();
			}
            catch (ArithmeticException eNull) {
            	System.out.println("Eingabe kann nicht 0 sein!");
            }
        }
    }
}