package ChristMess;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;

public class KindGenerator {
	
	private File file;
	
	public KindGenerator(File file) {
		this.file = file;
	}

	ArrayList<Kind> kinderliste = new ArrayList<Kind>();
	
	// Hier eine Methode erstellen, um die Datei der Kinder auszulesen und mit den
	// Daten eine Liste an Kind-Objekte anlegen

	public ArrayList<Kind> generateKinderListe() {
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader br = new BufferedReader(fr);
			String s;
			while ((s = br.readLine()) != null) {
				Kind k = kindhinzufügen(s);
				kinderliste.add(k);
			}
			br.close();
		} catch (Exception e) {
			System.out.println("File wahrscheinlich nicht vorhanden");
			e.printStackTrace();
		}
		return kinderliste;
	}
	
	public Kind kindhinzufügen(String a) {
		String s = a.replace(",","");
		String[] liste = s.split(" ");
		try {
			for (int i = 5; i < liste.length; i++) {
				liste[4] += liste[i];
			}
		return new Kind(liste[0], liste[1], liste[2], Integer.parseInt(liste[3]), liste[4]);
		
		}catch(Exception e) {
			System.out.println(a);
			e.printStackTrace();
			return null;
		}
	}
}
