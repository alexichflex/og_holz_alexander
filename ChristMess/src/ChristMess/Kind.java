package ChristMess;

import java.util.*;

public class Kind implements Comparable<Kind>{
	// Jedes Kind hat einen Nachnamen, Vornamen, Geburtstag, Wohnort und
	// Bravheitsgrad
	// Erstellen Sie einen vollparametrisierten Konstruktor, Getter/Setter und eine
	// toString-Methode

	private String vorname, name, ort, geburtstag;
	private int bravheitsgrad;

	
	public Kind(String vorname, String name, String geburtstag, int bravheitsgrad, String ort) {
		super();
		this.vorname = vorname;
		this.name = name;
		this.geburtstag = geburtstag;
		this.bravheitsgrad = bravheitsgrad;
		this.ort = ort;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGeburtstag() {
		return geburtstag;
	}

	public void setGeburtstag(String geburtstag) {
		this.geburtstag = geburtstag;
	}

	public int getBravheitsgrad() {
		return bravheitsgrad;
	}

	public void setBravheitsgrad(int bravheitsgrad) {
		this.bravheitsgrad = bravheitsgrad;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}
	@Override
	public int compareTo(Kind o) {
		// TODO Auto-generated method stub
		if(getBravheitsgrad() != o.bravheitsgrad) {
			return o.bravheitsgrad - getBravheitsgrad();
		}
		return getOrt().compareTo(o.ort);
	}

	@Override
	public String toString() {
		return "Kind [vorname=" + vorname + ", name=" + name + ", ort=" + ort + ", geburtstag=" + geburtstag
				+ ", bravheitsgrad=" + bravheitsgrad + "]";
	}

}