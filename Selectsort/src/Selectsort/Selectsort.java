package Selectsort;

import java.util.Scanner;

public class Selectsort {

	private static Scanner scan;

	public static int[] selectionsort(int[] sortieren) {
		for (int i = 0; i < sortieren.length - 1; i++) {
			for (int j = i + 1; j < sortieren.length; j++) {
				if (sortieren[i] > sortieren[j]) {
					int temp = sortieren[i];
					sortieren[i] = sortieren[j];
					sortieren[j] = temp;
				}
			}
		}

		return sortieren;
	}

	public static void main(String[] args) {

		int auswahl;
		do {
			int[] unsortiert = { 16, 23, 14, 7, 21, 20, 6, 1, 17, 13, 12, 9, 3, 19 };
			int[] sortiert = selectionsort(unsortiert);

			System.out.println("Selectsort - Start:");
			for (int i = 0; i < sortiert.length; i++) {
				System.out.println(sortiert[i] + ",");
			}
			System.out.println("Ende");
			System.out.println("Wollen Sie wiederholen? 1 f�r ja, 0 f�r nein");
			scan = new Scanner(System.in);
			auswahl = scan.nextInt();
		} while (auswahl == 1);
	}
}
