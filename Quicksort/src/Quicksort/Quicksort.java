package Quicksort;

import java.util.Scanner;

public class Quicksort {
	
	private static Scanner scan;

	public static int[] intArr = { 1, 3, 6, 7, 9, 12, 13, 14, 16, 17, 19, 20, 21, 23 };

	public int[] sort(int l, int r) {
		int q;
		if (l < r) {
			q = partition(l, r);
			sort(l, q);
			sort(q + 1, r);
		}
		return intArr;
	}

	int partition(int l, int r) {

    	int i, j, x = intArr[(l + r) / 2];
        i = l - 1;
        j = r + 1;
        while (true) {
            do {
                i++;
            } while (intArr[i] < x);

            do {
                j--;
            } while (intArr[j] > x);

            if (i < j) {
                int k = intArr[i];
                intArr[i] = intArr[j];
                intArr[j] = k;
            } else {
                return j;
            }
        }
    }

	public static void main(String[] args) {int auswahl;
		
	do {
        Quicksort qs = new Quicksort();
        int[] arr = qs.sort(0, intArr.length - 1);
        System.out.println("Quicksort - Start:");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i] + ",");
        }
        System.out.println("Ende");
		System.out.println("Wollen Sie wiederholen? 1 f�r ja, 0 f�r nein");
		scan = new Scanner(System.in);
		auswahl = scan.nextInt();
	}while(auswahl==1);
}
}