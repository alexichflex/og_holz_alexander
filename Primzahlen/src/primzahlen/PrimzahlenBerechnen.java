//Modulo
package primzahlen;

import java.util.Scanner;

public class PrimzahlenBerechnen {

	private static Scanner scan;
	private static boolean istPrimzahl;
	private static Scanner scan2;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int auswahl;
		do {
			int rest = 0;
			System.out.println("Bitte Zahl eingeben: ");
			istPrimzahl = true;
			scan = new Scanner(System.in);
			long zahl1 = scan.nextLong();
			StoppuhrFuerPrim uhr1 = new StoppuhrFuerPrim();
			uhr1.start();
			for (long i = 1; i <= zahl1; i++) {
				if (zahl1 % i == 0) {
					rest++;
					if (rest >= 3) {
						istPrimzahl = false;
						break;
					}
				}
			}
			uhr1.stopp();
			if (istPrimzahl) {
				System.out.println("Die Zahl " + zahl1 + " ist eine Primzahl!");
			} else {
				System.out.println("Die Zahl " + zahl1 + " ist keine Primzahl!");
			}
			System.out.println(uhr1.getDauerInMs() + "ms");
			System.out.println("Wollen Sie wiederholen? 1 f�r ja, 0 f�r nein");
			scan2 = new Scanner(System.in);
			auswahl = scan2.nextInt();
		} while (auswahl == 1);
	}
}
