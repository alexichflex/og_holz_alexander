package schleifen;

import java.util.Scanner;

public class Schleifentest {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int anzahl;
		System.out.println("Bitte geben sie eine Zahl ein: ");
		anzahl = scan.nextInt();

		// Pluszeichen generieren
		for (int i = 0; i < anzahl; i++) {
			System.out.print("+");
		}

		// Minuszeichen generieren
		int i = 0;
		while (i < anzahl) {
			System.out.print("-");
			i++;
		}
		// Malzeichen generieren
		i = 0;
		do {
			System.out.print("*");
			i++;
		} while (i < anzahl);
	}

}