package git_ZooTycoon;

public class Addon {

	private int idNummer;
	private double preis;
	private String bezeichnung;
	private int maxstock;
	private double owned;

	public Addon(int idNummer, double preis, String bezeichnung, int maxstock, double owned) {
		this.idNummer = idNummer;
		this.preis = preis;
		this.bezeichnung = bezeichnung;
		this.maxstock = maxstock;
		this.owned = owned;
	}

	public int getIdNummer() {
		return idNummer;
	}

	public void setIdNummer(int idNummer) {
		this.idNummer = idNummer;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getMaxstock() {
		return maxstock;
	}

	public void setMaxstock(int maxstock) {
		this.maxstock = maxstock;
	}

	public double getOwned() {
		return owned;
	}

	public void setOwned(double owned) {
		this.owned = owned;
	}

	public void andereBestand() {

	}

}
