package Protokoll;

import java.util.Random;

public class Klausur2 {

	public final int NICHT_GEFUNDEN = -1;

	public static void main(String[] args) {
		Klausur2 k2 = new Klausur2();
		int n = 1000; // Anzahl der Stellen

		// Erstellt und sortiert Array mit n Stellen
		long[] zahlen = k2.getSortedIntArray(n);

		// ----------Hier in die Main k�nnen Sie Ihre Analyse f�r das Protokoll
		// schreiben--------------

	}

	public int lineareSuche(long[] zahlen, long gesuchteZahl) {
		int anzahl = 0;
		for (int i = 0; i < zahlen.length; i++)
			if (zahlen[i] == gesuchteZahl)
				return i;
		anzahl++;
		return anzahl;
	}

	public int schlaubiSchlumpfSuche(long[] zahlen, long gesuchteZahl) {
		int anzahl2 = 0;
		int l = zahlen.length/4 - 1;
		while(l < zahlen.length && l >= 0){
			if(gesuchteZahl == zahlen[l]) {
				anzahl2++;
				return l;
			if(gesuchteZahl < zahlen [l]) {
				anzahl2++;
				l--;
		}
			}
			else {
				l = l + zahlen.length/4;
			}
		return anzahl2;
	}

	public int binaereSuche(long[] zahlen, long gesuchteZahl) {
		int anzahl3 = 0;
		int l = 0, r = zahlen.length - 1;
		int m;

		while (l <= r) {
			// Bereich halbieren
			m = l + ((r - l) / 2);

			if (zahlen[m] == gesuchteZahl) // Element gefunden?
				anzahl3++;
				return m;

			if (zahlen[m] > gesuchteZahl)
				r = m - 1; // im linken Abschnitt weitersuchen
			anzahl3++;
			else
				l = m + 1; // im rechten Abschnitt weitersuchen
			anzahl3++;
		}
		return anzahl3;
	}

	// -----------------------Finger weg von diesem Teil des Codes

	/**
	 * Methode f�llt das Attribut zahlen mit einem Array der L�nge des Paramaters
	 * z.B. getIntArray(1000) gibt ein 1000-stelliges Array zur�ck
	 * 
	 * @param stellen
	 *            - Anzahl der zuf�lligen Stellen
	 * @return das Array
	 */
	public long[] getSortedIntArray(int stellen) {
		Random rand = new Random(666L);
		long[] zahlen = new long[stellen];
		for (int i = 0; i < stellen; i++)
			zahlen[i] = rand.nextInt(20000000);
		this.radixSort(zahlen);
		return zahlen;
	}

	/**
	 * Super effizienter Algorithmus zur Sortierung der Zahlen Laufzeit von
	 * //NoHintAvailable
	 */
	public void radixSort(long[] zahlen) {
		long[] temp = new long[20000000];
		for (long z : zahlen)
			temp[(int) z]++;
		int stelle = 0;
		for (int i = 0; i < temp.length; i++)
			if (temp[i] != 0)
				while (temp[i]-- != 0)
					zahlen[stelle++] = i;
	}

}
