package TORnado;

public class Spiel {

	// Attribute
	private boolean warHeimmannschaft;
	private String spielzeit;
	private String ergebnis;
	private Mannschaft mannschaft;

	// Methoden
	public Spiel() {
		// TODO Auto-generated constructor stub
	}

	public Spiel(boolean warHeimmannschaft, String spielzeit, String ergebnis, Mannschaft m) {
		this.warHeimmannschaft = warHeimmannschaft;
		this.spielzeit = spielzeit;
		this.ergebnis = ergebnis;
		this.mannschaft = m;
	}

	public boolean isWarHeimmannschaft() {
		return warHeimmannschaft;
	}

	public void setWarHeimmannschaft(boolean warHeimmannschaft) {
		this.warHeimmannschaft = warHeimmannschaft;
	}

	public String getSpielzeit() {
		return spielzeit;
	}

	public void setSpielzeit(String spielzeit) {
		this.spielzeit = spielzeit;
	}

	public String getErgebnis() {
		return ergebnis;
	}

	public void setErgebnis(String ergebnis) {
		this.ergebnis = ergebnis;
	}

	public Mannschaft getMannschaft() {
		return mannschaft;
	}

	public void setMannschaft(Mannschaft mannschaft) {
		this.mannschaft = mannschaft;
	}

}