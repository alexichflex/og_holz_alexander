package TORnado;

public class Spieler extends Mitglieder {

	// Attribute
	private int trikotnummer;
	private String spielposition;
	private Mannschaft mannschaft;

	// Methoden
	public Spieler() {
		// TODO Auto-generated constructor stub
	}

	public Spieler(String name, double telefonnummer, boolean jahresbeitragGezahlt, int trikotnummer, String spielposition, Mannschaft m) {
		super(name, telefonnummer, jahresbeitragGezahlt);
		this.trikotnummer = trikotnummer;
		this.spielposition = spielposition;
		this.mannschaft = m;
	}

	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}

	public String getSpielposition() {
		return spielposition;
	}

	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}

	public Mannschaft getMannschaft() {
		return mannschaft;
	}

	public void setMannschaft(Mannschaft mannschaft) {
		this.mannschaft = mannschaft;
	}

}