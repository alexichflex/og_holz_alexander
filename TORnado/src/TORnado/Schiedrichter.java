package TORnado;

public class Schiedrichter extends Mitglieder {

	// Attribute
	private String nameSchieri;
	private int anzSpiele;

	// Methoden
	public Schiedrichter() {
		// TODO Auto-generated constructor stub
	}

	public Schiedrichter(String name, double telefonnummer, boolean jahresbeitragGezahlt, String nameSchieri, int anzSpiele) {
		super(name, telefonnummer, jahresbeitragGezahlt);
		this.nameSchieri = nameSchieri;
		this.anzSpiele = anzSpiele;
	}

	public String getNameSchieri() {
		return nameSchieri;
	}

	public void setNameSchieri(String nameSchieri) {
		this.nameSchieri = nameSchieri;
	}

	public int getAnzSpiele() {
		return anzSpiele;
	}

	public void setAnzSpiele(int anzSpiele) {
		this.anzSpiele = anzSpiele;
	}

}