package TORnado;

public class Mannschaftsleiter extends Spieler {

	// Attribute
	private String nameMannschaft;
	private double rabbatJahresbeitrag;

	// Methoden
	public Mannschaftsleiter() {
		// TODO Auto-generated constructor stub
	}

	public Mannschaftsleiter(String name, double telefonnummer, boolean jahresbeitragGezahlt, int trikotnummer, String spielposition, Mannschaft mannschaft, String nameMannschaft, double rabbatJahresbeitrag) {
		super(name, telefonnummer, jahresbeitragGezahlt, trikotnummer, spielposition, mannschaft);
		this.nameMannschaft = nameMannschaft;
		this.rabbatJahresbeitrag = rabbatJahresbeitrag;
	}

	public String getNameMannschaft() {
		return nameMannschaft;
	}

	public void setNameMannschaft(String nameMannschaft) {
		this.nameMannschaft = nameMannschaft;
	}

	public double getRabbatJahresbeitrag() {
		return rabbatJahresbeitrag;
	}

	public void setRabbatJahresbeitrag(double rabbatJahresbeitrag) {
		this.rabbatJahresbeitrag = rabbatJahresbeitrag;
	}

}