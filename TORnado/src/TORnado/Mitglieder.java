package TORnado;

public class Mitglieder {

	// Attribute
	private String name;
	private double telefonnummer;
	private boolean jahresbeitragGezahlt;

	// Methoden
	public Mitglieder() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Mitglieder(String name, double telefonnummer, boolean jahresbeitragGezahlt) {
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.jahresbeitragGezahlt = jahresbeitragGezahlt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(double telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public boolean isJahresbeitragGezahlt() {
		return jahresbeitragGezahlt;
	}

	public void setJahresbeitragGezahlt(boolean jahresbeitragGezahlt) {
		this.jahresbeitragGezahlt = jahresbeitragGezahlt;
	}

}