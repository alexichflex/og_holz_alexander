package TORnado;

import java.util.ArrayList;

public class Mannschaft {

	// Attribute
	private String name;
	private String spielklasse;
	private Trainer trainer;
	private Spieler spieler;
	private Spiel spiel;
	private ArrayList<Spiel> spielliste = new ArrayList<Spiel>();
	private ArrayList<Spieler> spielerliste;

	// Methoden
	public Mannschaft() {
		// TODO Auto-generated constructor stub
	}

	public Mannschaft(String name, String spielklasse, Trainer t, Spieler spieler, Spiel spiel, ArrayList<Spieler> sListe) {
		this.name = name;
		this.spielklasse = spielklasse;
		this.trainer = t;
		this.spieler = spieler;
		this.spiel = spiel;
		this.spielerliste = sListe;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpielklasse() {
		return spielklasse;
	}

	public void setSpielklasse(String spielklasse) {
		this.spielklasse = spielklasse;
	}

	public Trainer getTrainer() {
		return trainer;
	}

	public void setTrainer(Trainer trainer) {
		this.trainer = trainer;
	}

	public Spieler getSpieler() {
		return spieler;
	}

	public void setSpieler(Spieler spieler) {
		this.spieler = spieler;
	}

	public Spiel getSpiel() {
		return spiel;
	}

	public void setSpiel(Spiel spiel) {
		this.spiel = spiel;
	}

	public ArrayList<Spiel> getSpielliste() {
		return spielliste;
	}

	public void setSpielliste(ArrayList<Spiel> spielliste) {
		this.spielliste = spielliste;
	}

	public ArrayList<Spieler> getSpielerliste() {
		return spielerliste;
	}

	public void setSpielerliste(ArrayList<Spieler> spielerliste) {
		this.spielerliste = spielerliste;
	}
	
}