package TORnado;

import java.util.ArrayList;

public class Trainer extends Mitglieder {

	// Attribute
	private String lizenzklasse;
	private int aufwandsentschaedigung;
	private Mannschaft mannschaft;
	private ArrayList<Mannschaft> mannschaftliste = new ArrayList<Mannschaft>();

	// Methoden
	public Trainer() {
		// TODO Auto-generated constructor stub
	}

	public Trainer(String name, double telefonnummer, boolean jahresbeitragGezahlt, String lizenzklasse,
			int aufwandsentschaedigung, Mannschaft mannschaft) {
		super(name, telefonnummer, jahresbeitragGezahlt);
		this.lizenzklasse = lizenzklasse;
		this.aufwandsentschaedigung = aufwandsentschaedigung;
		this.mannschaft = mannschaft;
	}

	public String getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(String lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}

	public int getAufwandsentschaedigung() {
		return aufwandsentschaedigung;
	}

	public void setAufwandsentschaedigung(int aufwandsentschaedigung) {
		this.aufwandsentschaedigung = aufwandsentschaedigung;
	}

	public Mannschaft getMannschaft() {
		return mannschaft;
	}

	public void setMannschaft(Mannschaft mannschaft) {
		this.mannschaft = mannschaft;
	}

	public ArrayList<Mannschaft> getMannschaftliste() {
		return mannschaftliste;
	}

	public void setMannschaftliste(ArrayList<Mannschaft> mannschaftliste) {
		this.mannschaftliste = mannschaftliste;
	}

}