package kgv;

import static org.junit.Assert.*;

import org.junit.Test;

public class kgvTest {

	@Test
	public void test1() {
		long erg=kgv.kgv(3, 6);
		assertEquals(6, erg);
	}

	@Test
	public void test2() {
		long erg=kgv.kgv(568506, 33333);
		assertEquals(154065126, erg);
	}
	
	@Test
	public void test3() {
		long erg=kgv.kgv(5, 6485612);
		assertEquals(32428060, erg);
	}
	
	@Test
	public void test4() {
		long erg=kgv.kgv(1561, 75156);
		assertEquals(117318516, erg);
	}
	
	@Test
	public void test5() {
		long erg=kgv.kgv(37891, 7845);
		assertEquals(297254895, erg);
	}
	
	@Test
	public void test6() {
		long erg=kgv.kgv(18565, 14458);
		assertEquals(268412770, erg);
	}
	
	@Test
	public void test7() {
		long erg=kgv.kgv(100000, 5555);
		assertEquals(111100000, erg);
	}
}
