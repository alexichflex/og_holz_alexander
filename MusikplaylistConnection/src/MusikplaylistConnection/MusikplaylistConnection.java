package MusikplaylistConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class MusikplaylistConnection {

	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost/keks";
	private String user = "root";
	private String password = "";
	
	public boolean insertGaeste (int vote_id, String name, String vorname) {
		String sql = "INSERT INTO t_gaeste (bezeichnung, sorte) VALUES ('" +vote_id+ "', '"+name+"', '"+vorname+"');";
		
		try {
			//JDBC-Treiber laden
			Class.forName(driver);
			//Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			
			con.close();
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean insertMusik (String bandname, String titel, String genre, String anzahl_votes) {
		String sql = "INSERT INTO t_musik (Bandname, Titel, Genre, Anzahl_Votes) VALUES ('" +bandname+ "', '" +titel+ "', '"+genre+"', '"+anzahl_votes+"');";
		
		try {
			//JDBC-Treiber laden
			Class.forName(driver);
			//Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			
			con.close();
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean insertPlaylist (String musik_mit_meisten_votes) {
		String sql = "INSERT INTO t_playlist (Musik_mit_meisten_Votes) VALUES ('" +musik_mit_meisten_votes+"');";
		
		try {
			//JDBC-Treiber laden
			Class.forName(driver);
			//Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			
			con.close();
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean insertVotes (int f_lied_id, String uhrzeit_vote) {
		String sql = "INSERT INTO t_votes (F_Lied_ID, Uhrzeit_Vote) VALUES ('" +f_lied_id+ "', '"+uhrzeit_vote+"');";
		
		try {
			//JDBC-Treiber laden
			Class.forName(driver);
			//Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			
			con.close();
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		MusikplaylistConnection dbc = new MusikplaylistConnection();
		dbc.insertGaeste(0, "", "");
		dbc.insertMusik("", "", "", "");
		dbc.insertPlaylist("");
		dbc.insertVotes(0, "");
	}
}