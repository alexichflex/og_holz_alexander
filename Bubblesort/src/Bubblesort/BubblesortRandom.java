package Bubblesort;

public class BubblesortRandom {
	
	public static void main(String args[]) {
		int arr[] = new int[100000];
		double tmp;

		for (int i = 0; i < arr.length; i++) {
			tmp = 1000000 * Math.random();
			tmp = Math.floor(tmp);
			arr[i] = (int) tmp;
		}

		int temp;
		long time = System.currentTimeMillis();
		for (int i = 1; i < arr.length; i++)
			for (int j = arr.length - 1; j >= i; j--)
				if (arr[j - 1] > arr[j]) {
					temp = arr[j - 1];
					arr[j - 1] = arr[j];
					arr[j] = temp;
				}

		System.out.println("sortiert");
		for (int i = 0; i < arr.length; i++) 
			System.out.println(arr[i] + ",");
		System.out.println(System.currentTimeMillis() - time + "ms");
	}
}